package application;

import StageMaster.StageMaster;
import db_Opration.DbUtil;
import javafx.application.Application;
import javafx.stage.Stage;
import show_Options.ShowOption;

public class ApplicationMain extends Application{
		public static void main(String args[]) {
			DbUtil.createDbConnection();
			launch(args);
			
		}
		public void start(Stage primaryStage) {
			StageMaster.setStage(primaryStage);
			new ShowOption().show();
		}
		

	}


