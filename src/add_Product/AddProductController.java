package add_Product;


import db_Opration.DbUtil;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import show_Options.ShowOption;

public class AddProductController {
	@FXML
	private TextField productName;
	@FXML
	private TextField quantity;
	@FXML
	private TextField productPrice;
	@FXML
	private Button addProductButton;

	public void saveProduct(ActionEvent event) {
		System.out.println(productName.getText());
		System.out.println(quantity.getText());
		System.out.println(productPrice.getText());

		String query = "insert into Product( Name, Quantity, Price) values('" + productName.getText() + "','"
				+ quantity.getText() + "','" + productPrice.getText() + "');";
		//System.out.println(query);
		DbUtil.executeQuery(query);
		new ShowOption().show();
	}
}
