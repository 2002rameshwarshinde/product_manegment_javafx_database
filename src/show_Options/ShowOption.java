package show_Options;

import java.io.IOException;

import StageMaster.StageMaster;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;

public class ShowOption {
	public static void main(String args[]) {

	}

	public void show() {
		try{
			Parent actorGroup =FXMLLoader.load(getClass().getResource("ShowOption.fxml"));
			StageMaster.getStage().setScene(new Scene(actorGroup));
			StageMaster.getStage().show();
		}catch(IOException e) {
			e.printStackTrace();
		}
		
		
	}

}
